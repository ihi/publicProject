A) Install Docker

B) Make Docker Image

    1. cd tensorflow
    2. docker build -t my-tensorflow ./

C) kick jupyter-notebook

    1. sh run_tensorflow.sh

    When running on VM. Foward the port 8888 and 6006 for jupyter-notebook and tensorboard.
